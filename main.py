import hashlib


class Node:
    def __init__(self, data: str):
        self.data_hash = hashlib.md5(data.encode())
        self.previous_data_hash = None
        self.data = data
        self.next = None

    def append(self, val: str):
        end = Node(val)
        n = self
        while n.next:
            n = n.next
        end.previous_data_hash = n.data_hash
        n.next = end


ll = Node('1')
ll.append('2')
ll.append('3')
ll.append('hello')
ll.append('world')
node = ll

print(node.data)
print(node.data_hash.hexdigest())
while node.next:
    node = node.next
    print(node.data)
    print("Собственная хэш сумма: ", node.data_hash.hexdigest())
    print("Предыдущая хэш сумма: ", node.previous_data_hash.hexdigest())

